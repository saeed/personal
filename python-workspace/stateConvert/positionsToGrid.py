gridX = 1
gridY = 1

fieldMaxX = 55
fieldMaxY = 36

def transformY(realy):
	realy+=fieldMaxY
	#realy/2
	realy/=gridY
	return realy

def transformX(realx):
	realx+=fieldMaxX
	realx/=gridX
	return fieldMaxX*2 - realx

def getGridIndexOfPosition(x,y):
	tx = transformX(x)
	ty = transformY(y)
	index = ty + tx*(fieldMaxY*2/gridY)
	return index
