from convertRawStateToNN import *

import os
import sys

inputMatrix = []
outputMatrix = []

directory = './states/'

for root,dirs,files in os.walk(directory):
	for file in files:
		if file.endswith('.txt'):
			f = open(directory+file,'r')
		#	print file
			[i,o] = converStateToNN(f)
			inputMatrix.append(i)
			outputMatrix.append(o)
			f.close()
			f = open(directory+file,'r')
			[i,o] = converConjugateStateToNN(f)
			inputMatrix.append(i)
			outputMatrix.append(o)
			f.close()

sys.stdout = open('inputFile.csv','w')
for inp in inputMatrix:	
	print ','.join(map(str,inp))
	#for x in inp:
	#	print x,
	#print

sys.stdout = open('outputFile.csv','w')	
for out in outputMatrix:
	print ','.join(map(str,out))
	#for x in out:
	#	print x,
	#print

#for out in output
#print inputMatrix
#print outputMatrix
