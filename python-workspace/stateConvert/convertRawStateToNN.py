import sys

from positionsToGrid import getGridIndexOfPosition

def converStateToNN( stateFile ):
	sys.stdin = stateFile
	try:
		scores = map(float,raw_input().split())
		#print scores
	except ValueError:
		scores = map(float,raw_input().split(','))
	overallScore = scores[-1]
	if 10<=overallScore or overallScore<0:
		print 'error'
	ball = map(float,raw_input().split())
	
	rightTeamIndex = []
	leftTeamIndex = []
	for _ in range(11):
		player = map(float,raw_input().split())
		leftTeamIndex.append( getGridIndexOfPosition(round(player[0]),round(player[1])) )
	for _ in range(11):
		player = map(float,raw_input().split())
		rightTeamIndex.append( getGridIndexOfPosition(round(player[0]),round(player[1])) )
	#print sorted(rightTeamIndex),sorted(leftTeamIndex)
	
	## print
	inputMatrix = []
	inputMatrix.append(getGridIndexOfPosition(round(ball[0]),round(ball[1])))
	inputMatrix.extend ( sorted(rightTeamIndex) )
	inputMatrix.extend ( sorted(leftTeamIndex) )
	outputMatrix = [overallScore]
#	print inputMatrix
#	print outputMatrix
	return [inputMatrix,outputMatrix]

	
def converConjugateStateToNN( stateFile ):
	
	def conjugate(pos):
		#~ print pos
		pos[1]*=-1
	
	sys.stdin = stateFile
	try:
		scores = map(float,raw_input().split())
		#print scores
	except ValueError:
		scores = map(float,raw_input().split(','))
	overallScore = scores[-1]
	ball = map(float,raw_input().split())
	conjugate(ball)
	rightTeamIndex = []
	leftTeamIndex = []
	for _ in range(11):
		player = map(float,raw_input().split())
		conjugate(player)
		leftTeamIndex.append( getGridIndexOfPosition(round(player[0]),round(player[1])) )
	for _ in range(11):
		player = map(float,raw_input().split())
		conjugate(player)
		rightTeamIndex.append( getGridIndexOfPosition(round(player[0]),round(player[1])) )
	#print sorted(rightTeamIndex),sorted(leftTeamIndex)
	
	## print
	inputMatrix = []
	inputMatrix.append(getGridIndexOfPosition(round(ball[0]),round(ball[1])))
	inputMatrix.extend ( sorted(rightTeamIndex) )
	inputMatrix.extend ( sorted(leftTeamIndex) )
	outputMatrix = [overallScore]
#	print inputMatrix
#	print outputMatrix
	return [inputMatrix,outputMatrix]

