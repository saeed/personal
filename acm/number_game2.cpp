#include <iostream>
#include <vector>
#include <cstring>

using namespace std;

int dp [25][25][25][25][25];

bool f ( int k1, int k2, int k3, int k4, int k5 ){
	cout << k1 << "," << k2 <<","<<k3<<","<<k4<<","<<k5 << endl;
	if ( dp[k1][k2][k3][k4][k5] != -1 )
		return dp[k1][k2][k3][k4][k5];
	if ( k1==0 && k2==0 ){
		//~ if ( k2 + k3 == 1 ){
			//~ if ( k5%2 )
				//~ return true;
			//~ else
				//~ return false;
		//~ }
		//~ else if ( k2 + k3 == 2 ){
			//~ 
		//~ }
		if ( k3 == k4 && k4 == 0 )
			return true;
		return ( k2+k3+k5 )%2==0;
	}
	bool b1 = true,b2 = true,b3 = true;
	if ( k1 > 0 && k2 > 0 ){
		b1 = dp [0][k2-1][k3][k4][k5+k1] = f ( 0,k2-1,k3,k4,k5+k1 );
	}
	if ( k3 > 0 && k4 > 0 ){
		b2 = dp [k1][k2][k3-1][0][k5+k4] = f (k1, k2,k3-1,0,k5+k4 );
	}
	if ( k5 > 0 )
		b3 = dp [k1][k2][k3][k4][k5-1] = f(k1,k2,k3,k4,k5-1);
	if ( !b1 || !b2 || !b3)
		return true;
	return false;
}

int main(){
	memset(dp,-1,sizeof(dp));
	int t;cin>>t;
	while(t--){
		int n; cin>>n;
		vector<int> v(n);
		int one_position ;
		for ( int i=0;i<n;++i ){
			cin>>v[i];
			if ( v[i]==1 ){
				one_position = i;
			}
		}
		int right_up = one_position;
		for ( int i = one_position+1; i<v.size(); ++i ){
			if ( i+1 >= v.size() || v[i+1]<v[i] ){
				right_up = i;
				break;
			}
		}
		int right_down = right_up;
		for ( int i = right_up+1; i<v.size(); ++i ){
			if ( i+1 >= v.size() || v[i+1]>v[i] ){
				right_down = i;
				break;
			}
		}
		int left_up = one_position;
		for ( int i = one_position-1; i>=0; --i ){
			if ( i-1 < 0 || v[i-1]<v[i] ){
				left_up = i;
				break;
			}
		}
		int left_down = left_up;
		for ( int i = left_up-1; i>=0; --i ){
			if ( i-1 < 0 || v[i-1]>v[i] ){
				left_down = i;
				break;
			}
		}
		int right_up_count = right_up - one_position;
		int left_up_count = one_position - left_up;
		int right_down_count = right_down - right_up;
		int left_down_count = left_up - left_down;
		int others = n - ( right_up_count + left_up_count + right_down_count + left_down_count + 1 );
		
		if ( f(left_down_count,left_up_count,right_up_count,right_down_count,others) )
			cout << "BOB" << endl;
		else
			cout << "ALICE" << endl;
		//~ cout << f(left_down_count,left_up_count,right_up_count,right_down_count,others) << endl;
	}
}

