#include <iostream>
#include <vector>

using namespace std;

int main(){
	int t;cin>>t;
	while(t--){
		int n; cin>>n;
		vector<int> v(n);
		int one_position ;
		for ( int i=0;i<n;++i ){
			cin>>v[i];
			if ( v[i]==1 ){
				one_position = i;
			}
		}
		int right_position = one_position;
		for ( int i = one_position+1; i<v.size(); ++i ){
			if ( i+1 >= v.size() || v[i+1]<v[i] ){
				right_position = i;
				break;
			}
		}
		int left_position = one_position;
		for ( int i = one_position-1; i>=0; --i ){
			if ( i-1 < 0 || v[i-1]<v[i] ){
				left_position = i;
				break;
			}
		}
		int right_count = right_position - one_position;
		int left_count = left_position - one_position;
		int others = n - (right_count - left_count - 1 );
		int middle = right_count + left_count;
		if ( middle == 1 ){
			if ( others%2 == 0 )
				cout << "BOB" << endl;
			else
				cout << "ALICE" << endl;
		}
		else if ( right_count + left_count == 2 ){
			if ( (n-right_position) % 2 || left_position % 2 )
				cout << "ALICE" << endl;
			else
				cout << "BOB" << endl;
		} else {
			if ( middle + others % 2 == 0 ) {
				cout << "ALICE" << endl;
			} else {
				cout << "BOB" << endl;
			}
		}
	}
}
